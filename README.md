
## OMniLeads ACD customizations examples

When it comes to customizing the dialplan or any Asterisk configuration involving working with existing .conf files within /etc/asterisk, as well as when adding AGI files to the /var/lib/asterisk/agi-bin directory, in the realm of modern container-based applications, manipulating files within the container is not a recommended practice as it breaks the 'stateless' principle, meaning no reliance on what exists in the host file system where the container runs.

This is why, in OMniLeads 2.0, customizing Asterisk involves a new approach based on [multi stage build](https://docs.docker.com/build/building/multi-stage/) 'multi-stage' builds that allow work with Dockerfiles. In other words, you can start from the official omnileads-asterisk image (omnileads/asterisk:231125.01) and add layers with, for example, .conf files and AGIs.

Let's get to work. Suppose we have 2 clients that require dialplan and AGI customizations, which we'll call customer_1 and customer_2. As we assume we're operating in the DevOps paradigm, we need to create a Git repository to maintain and have traceability over the source code we generate specifically for these clients.

## Code

At the root of the repository, we have the Dockerfiles that build the Asterisk image we want to deploy. Meanwhile, in the 'tenant_1' and 'tenant_2' directories, we have the files containing the customized code for each client.

Each .conf file that you want to customize should exist within the directory corresponding to the respective tenant. For example, if you want to extend dialplan functionality, you should host an altered version of the oml_extensions_custom.conf file in that directory and use it to write the code that deploys or includes the functionality from another file.

Here, we have a couple of examples that will surely help incorporate the concept.

## Build

To proceed with the image build, we can approach it manually or take advantage of a technology that allows us to manage the build in a more automated way, such as GitLab CI/CD.

In the case of using GitLab CI/CD, we need to configure the Docker username (DOCKER_USERNAME) and password (DOCKER_SECRET) as CI/CD variables (Settings - CICD - Variables) for the container registry to which we want to push our images.

Si deseas construir manualmente tus imagenes:

**Tenant 1:**
```
docker build --file=Dockerfile_customer_1 --tag=my_custom_repo/asterisk:231212.01-customer_1 .
docker push my_custom_repo/asterisk:231212.01-customer_1
```

**Tenant 2:**
```
docker build --file=Dockerfile_customer_2 --tag=my_custom_repo/asterisk:231212.01-customer_2 .
docker push my_custom_repo/asterisk:231212.01-customer_2
```


## Deploy

When triggering a deploy with our customized images, we simply need to reference them either in our .env file for Docker Compose environments or in inventory.yml for Ansible/Podman.


* docker-compose: Deploying a custom Asterisk image using Docker Compose

```
OMLACD_IMG=my_custom_repo/asterisk:231125.01
```

* Ansible/Podman: Deploying a custom Asterisk image using Ansible / Podman

```
tenant_example_1:
    tenant_id: tenant_example_1
    ansible_host: 24.199.100.212 
    omni_ip_lan: 10.10.10.5
    img_registry_omlacd_url: docker.io
    img_registry_omlacd_repo: my_custom_repo
    asterisk_version: 31212.01-tenant_1
```
