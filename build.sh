#!/bin/bash

set -e

IMAGE_VERSION=$(cat version.txt | cut -d':' -f2)  
IMAGE_REPO=$(cat repo.txt)
docker build --build-arg FROM_IMG_TAG=$(cat version.txt) --file=Dockerfile_$1 --tag=$IMAGE_REPO:$IMAGE_VERSION.$1 .
docker push $IMAGE_REPO:$IMAGE_VERSION.$1