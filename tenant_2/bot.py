#!/usr/local/bin/python3

import os
import sys
from asterisk.agi import AGI

def main():
    agi = AGI()
    agi.answer()

    try:        
        agi.stream_file('hello-world')  
        agi.set_variable('AGI_VARIABLE', 'HELLO WORLD from AGI !')
    except AGIException as e:
        agi.verbose("AGI error occurred: " + str(e))
    except Exception as e:
        agi.verbose("General error occurred: " + str(e))

if __name__ == "__main__":
    main()
